name := "stopLoss"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % "0.13.0",
  "io.circe" %% "circe-generic" % "0.13.0",
  "io.circe" %% "circe-generic-extras" % "0.13.0",
  "com.softwaremill.sttp.client" %% "core" % "2.1.1",
  "com.softwaremill.sttp.client" %% "circe" % "2.1.1",
  "com.softwaremill.sttp.client" %% "async-http-client-backend-cats" % "2.1.1",
  "com.softwaremill.sttp.client" %% "async-http-client-backend-fs2" % "2.1.1",
  "com.couchbase.client" %% "scala-client" % "1.0.3"
    exclude("com.lihaoyi", "upickle_2.12")
    excludeAll ExclusionRule(organization = "io.netty"),
  "com.github.pureconfig" %% "pureconfig" % "0.12.3",
  "com.beachape" %% "enumeratum" % "1.5.15",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.scalatest" %% "scalatest" % "3.0.8" % Test
)

//scalacOptions ++= Seq(
//  "-Ywarn-dead-code",                  // Warn when dead code is identified.
//  "-Ywarn-value-discard",              // Warn when non-Unit expression results are unused.
//
//  "-unchecked",
//  "-feature",
//  "-deprecation:false",
//  "-Xfatal-warnings",
//)

assemblyMergeStrategy in assembly := {
  case x if x.contains("io.netty.versions.properties") => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}


