import cats.effect.{ContextShift, IO, Timer}
import fs2.Stream
import ml.tradeapi.model.Streaming.{OrderBookEvent, OrderBookSubscribe, OrderBookUnsubscribe}
import ml.tradeapi.model.{PendingOrder, User}
import ml.tradeapi.services.{StopLoss, UserServiceAsync}
import ml.tradeapi.streaming.HandlerIO
import ml.tradeapi.util.{Logging, Properties}
import sttp.client.asynchttpclient.fs2.{AsyncHttpClientFs2Backend, Fs2WebSocketHandler}
import sttp.client.{basicRequest, _}
import sttp.client.impl.fs2.Fs2WebSockets
import sttp.client.ws.{WebSocket, WebSocketResponse}
import sttp.model.ws.WebSocketFrame

import scala.concurrent.duration._

class WebSocketFsApp

object WebSocketFsApp extends App {
  import cats.implicits._

  import scala.concurrent.ExecutionContext.global

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)
  implicit val properties = Properties.properties.tinkoffApi

  val logging = Logging(classOf[WebSocketFsApp])

  val app = for {
    users <- UserServiceAsync.getAllUsers
    ws <- (users map createWs).parSequence_
  } yield ws

  app.unsafeRunSync()

  private def handleStream(handlerIO: HandlerIO, input: Stream[IO, WebSocketFrame.Incoming]): Stream[IO, WebSocketFrame] = {
    input.evalMap {
      case data: WebSocketFrame.Data[_] =>
        logging.debug(s"got string from ${handlerIO.user.telegramId} payload: ${data.payload.toString}") >>
        handleTextPayload(handlerIO, data.payload.toString)
      case WebSocketFrame.Ping(_) => logging.debug(s"Got Ping from ${handlerIO.user.telegramId} ws")
      case WebSocketFrame.Pong(_) => logging.debug(s"Got Pong from ${handlerIO.user.telegramId} ws")
      case _ => logging.warn(s"Got Nothing from ${handlerIO.user.telegramId} ws")
    }.drain
  }

  private def handleTextPayload(handler: HandlerIO, payload: String) = {
    import io.circe.generic.auto._
    import io.circe.parser._
    UserServiceAsync.getUserSubscriptions(handler.user.telegramId).flatMap {
      orders =>
        val event = parse(payload).toOption.flatMap(_.as[OrderBookEvent].toOption)
        event match {
          case Some(value) => StopLoss.handleEventIO(handler, orders, value)
          case None => IO.unit
        }
    }
  }

  def createWs(user: User): IO[Unit] = {
    AsyncHttpClientFs2Backend[IO]().flatMap { implicit backend =>
      val response: IO[WebSocketResponse[WebSocket[IO]]] = basicRequest
        .header("Authorization", s"Bearer ${user.token}")
        .get(uri"${properties.streaming}")
        .openWebsocketF(Fs2WebSocketHandler[IO]())

      response.flatMap { response =>
        val hnd = new HandlerIO(user, backend, response.result)
        val str = Fs2WebSockets.handleSocketThroughPipe(response.result) { handleStream(hnd, _) }
        val chk = check(Nil, hnd).map(_ => ())
        (chk, str).parMapN( (_, _) => ())
      }
    }
  }


  def checkSubscriptions(userId: Long, handler: HandlerIO, orders: List[PendingOrder]): IO[List[PendingOrder]] = {
    for {
      current <- UserServiceAsync.getUserSubscriptions(userId)
      _ <- current.diff(orders).map { order =>
        logging.info(s"Got new $order on $userId") >>
          handler.subscribe(OrderBookSubscribe(figi = order.figi))
      }.parSequence
      _ <- orders.diff(current).map { order =>
        logging.info(s"$order was removed on $userId") >>
          handler.unsubscribe(OrderBookUnsubscribe(figi = order.figi))
      }.parSequence
    } yield current
  }

  def check(orders: List[PendingOrder], handler: HandlerIO): IO[List[PendingOrder]] =
    for {
      _ <- IO.sleep(1.second)
      subscriptions <- checkSubscriptions(handler.user.telegramId, handler, orders)
      check_ <- check(subscriptions, handler)
    } yield (check_)
}
