import cats.effect.{ContextShift, IO, Timer}
import ml.tradeapi.services.{CouchBaseOffsetStore, SimpleChatService}
import sttp.client.asynchttpclient.fs2.AsyncHttpClientFs2Backend

import scala.concurrent.ExecutionContext.global

object App extends App {

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)

  AsyncHttpClientFs2Backend[IO]().flatMap { implicit backend =>

    val chatService = new SimpleChatService(new CouchBaseOffsetStore())
    chatService.simpleRun
  }.unsafeRunSync()
}