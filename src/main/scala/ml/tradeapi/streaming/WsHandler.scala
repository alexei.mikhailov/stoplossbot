package ml.tradeapi.streaming

import cats.effect.IO
import ml.tradeapi.model.Streaming.{OrderBookSubscribe, OrderBookUnsubscribe}
import ml.tradeapi.util.{Logging, sttpBackend}
import sttp.model.ws.WebSocketFrame
import io.circe.syntax._
import ml.tradeapi.model.User

class HandlerIO(val user: User,
                val backend: sttpBackend,
                ws: sttp.client.ws.WebSocket[IO]) {
  import cats.implicits._

  val logging = Logging(classOf[HandlerIO])

  def subscribe(subscription: OrderBookSubscribe): IO[Unit] =
    logging.info(s"Subscribe: $subscription") >>
    ws.send(WebSocketFrame.text(subscription.asJson.toString))

  def unsubscribe(subscription: OrderBookUnsubscribe): IO[Unit] =
    logging.info(s"Unsubscribe: $subscription") >>
    ws.send(WebSocketFrame.text(subscription.asJson.toString))
}