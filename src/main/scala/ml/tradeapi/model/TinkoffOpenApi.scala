package ml.tradeapi.model

import java.time.ZonedDateTime

import enumeratum._
import io.circe._
import io.circe.syntax._
import io.circe.generic.extras.semiauto.{deriveEnumerationDecoder, deriveEnumerationEncoder}
import io.circe.generic.semiauto.deriveEncoder
import io.circe.generic.semiauto.deriveDecoder

import scala.collection.immutable

sealed trait Currency extends EnumEntry
object Currency extends Enum[Currency]{
  case object EUR extends Currency
  case object USD extends Currency
  case object RUB extends Currency
  case object GBP extends Currency
  case object HKD extends Currency
  case object CHF extends Currency
  case object JPY extends Currency
  case object CNY extends Currency
  case object TRY extends Currency


  implicit val decoder: Decoder[Currency] = deriveEnumerationDecoder
  implicit val encoder: Encoder[Currency] = deriveEnumerationEncoder

  override def values: immutable.IndexedSeq[Currency] = findValues
}

case class PortfolioCurrency(currency: Currency, balance: BigDecimal, blocked: Option[BigDecimal] = None)

sealed trait OperationType
object OperationType {
  case object Buy extends OperationType
  case object Sell extends OperationType

  case object BuyStopLoss extends OperationType
  case object BuyTakeProfit extends OperationType

  implicit val decoder: Decoder[OperationType] = deriveEnumerationDecoder
  implicit val encoder: Encoder[OperationType] = deriveEnumerationEncoder
}

sealed trait OrderStatus extends EnumEntry
object OrderStatus extends Enum[OrderStatus] {
  case object New extends OrderStatus
  case object PartiallyFill extends OrderStatus
  case object Fill extends OrderStatus
  case object Cancelled extends OrderStatus
  case object Replaced extends OrderStatus
  case object PendingCancel extends OrderStatus
  case object Rejected extends OrderStatus
  case object PendingReplace extends OrderStatus
  case object PendingNew extends OrderStatus

  override def values = findValues

  implicit val decoder: Decoder[OrderStatus] = deriveEnumerationDecoder
  implicit val encoder: Encoder[OrderStatus] = deriveEnumerationEncoder
}

sealed trait InstrumentType extends EnumEntry
object InstrumentType extends Enum[InstrumentType] {
  case object Stock extends InstrumentType
  case object Currency extends InstrumentType
  case object Bond extends InstrumentType
  case object Etf extends InstrumentType

  override def values = findValues

  implicit val decoder: Decoder[InstrumentType] = deriveEnumerationDecoder
  implicit val encoder: Encoder[InstrumentType] = deriveEnumerationEncoder
}

sealed trait OrderType extends EnumEntry
object OrderType extends Enum[OrderType] {
  case object Limit extends OrderType
  case object Market extends OrderType
  override def values = findValues

  implicit val decoder: Decoder[Currency] = deriveEnumerationDecoder
  implicit val encoder: Encoder[Currency] = deriveEnumerationEncoder
}

object RequestTypes {
  case class addCurrency(currency: Currency, balance: BigDecimal)

  case class MarketOrder(lots: BigDecimal, operation: OperationType)
  case class LimitOrder(lots: BigDecimal, operation: OperationType, price: BigDecimal)
}

case class MoneyAmount(currency: Currency, value: BigDecimal)

case class Order(orderId: String, figi: String, operation: OperationType, status: OrderStatus,
                       requestedLots: Int, executedLots: Int, orderType: OrderType, price: BigDecimal)


case class MarketInstrument(figi: String, ticker: String, isin: Option[String], minPriceIncrement: Option[BigDecimal],
                            lot: Int, currency: Option[Currency], name: String)

case class PortfolioPosition(figi: String, ticker: Option[String], isin: Option[String], instrumentType: InstrumentType,
                             balance: BigDecimal, blocked: Option[BigDecimal], lots: Int, name: String)

case class Operation(id: String, operationType: OperationType, figi: String,
                     price: Option[BigDecimal], status: Option[String], date: ZonedDateTime)


sealed trait Payload
object Payload {
  import cats.syntax.functor._
  import io.circe.generic.auto._

  case class PortfolioCurrencies(currencies: List[PortfolioCurrency]) extends Payload
  case class CreateAccount(brokerAccountType: String, brokerAccountId: String) extends Payload
  case class ErrorPayload(message: String, code: String) extends Payload
  case class EmptyPayload() extends Payload
  case class PlacedOrderPayload(orderId: String, operation: OperationType, status: OrderStatus,
                                rejectReason: Option[String], message: Option[String],
                                requestedLots: Int, executedLots: Int, commission: Option[MoneyAmount]) extends Payload
  case class OrderListPayload(orders: List[Order]) extends Payload
  case class InstrumentsResponse(instruments: List[MarketInstrument]) extends Payload
  case class PortfolioResponse(positions: List[PortfolioPosition]) extends Payload
  case class OperationsResponse(operations: List[Operation]) extends Payload

  implicit val decodeEvent: Decoder[Payload] =
    List[Decoder[Payload]](
      Decoder[PortfolioCurrencies].widen,
      Decoder[ErrorPayload].widen,
      Decoder[CreateAccount].widen,
      Decoder[InstrumentsResponse].widen,
      Decoder[PlacedOrderPayload].widen,
      Decoder[OrderListPayload].widen,
      Decoder[PortfolioResponse].widen,
      Decoder[OperationsResponse].widen,
      Decoder[EmptyPayload].widen
    ).reduceLeft(_ or _)

  implicit val encoder: Encoder[Payload] = Encoder.instance {
    case PortfolioCurrencies @ PortfolioCurrencies(_) => PortfolioCurrencies.asJson
    case CreateAccount @ CreateAccount(_, _) => CreateAccount.asJson
    case InstrumentsResponse @ InstrumentsResponse(_) => InstrumentsResponse.asJson
    case ErrorPayload @ ErrorPayload(_, _) => ErrorPayload.asJson
    case EmptyPayload @ EmptyPayload() => EmptyPayload.asJson
    case OrderListPayload @ OrderListPayload(_) => OrderListPayload.asJson
    case PlacedOrderPayload @ PlacedOrderPayload(_, _,_ , _, _, _, _, _) => PlacedOrderPayload.asJson
    case PortfolioResponse @ PortfolioResponse(_) => PortfolioResponse.asJson
    case OperationsResponse @ OperationsResponse(_) => OperationsResponse.asJson
  }
}

case class ApiResponse(trackingId: String, status: String, payload: Payload)
object ApiResponse {
  implicit val decoder: Decoder[ApiResponse] = deriveDecoder
  implicit val encoder: Encoder[ApiResponse] = deriveEncoder
}
