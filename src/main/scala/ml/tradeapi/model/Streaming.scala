package ml.tradeapi.model

import java.time.ZonedDateTime

import io.circe.{Decoder, Encoder, HCursor, Json}
import io.circe.generic.extras.semiauto.{deriveEnumerationDecoder, deriveEnumerationEncoder}
import io.circe.generic.semiauto._

object Streaming {
  case class OrderBookSubscribe(event: String = "orderbook:subscribe", figi: String, depth: Int = 5, request_id: Option[String] = None)
  case class OrderBookUnsubscribe(event: String = "orderbook:unsubscribe", figi: String, depth: Int = 5, request_id: Option[String] = None)

  implicit val orderBookSubscribeEncoder: Encoder[OrderBookSubscribe] = deriveEncoder
  implicit val orderBookUnsubscribeEncoder: Encoder[OrderBookUnsubscribe] = deriveEncoder

  implicit val orderBookSubscribeDecoder: Decoder[OrderBookSubscribe] = deriveDecoder
  implicit val orderBookUnsubscribeDecoder: Decoder[OrderBookUnsubscribe] = deriveDecoder


  sealed trait EventType
  object EventType {
    case object orderbook extends EventType

    implicit val decoder: Decoder[EventType] = deriveEnumerationDecoder
    implicit val encoder: Encoder[EventType] = deriveEnumerationEncoder
  }

  case class OrderBook(price: BigDecimal, lot: BigDecimal)
  object OrderBook {
    implicit val encoder: Encoder[OrderBook] = new Encoder[OrderBook] {
      final def apply(orderBook: OrderBook): Json = Json.arr(
        Json.fromBigDecimal(orderBook.price),
        Json.fromBigDecimal(orderBook.lot)
      )
    }

    implicit val decoder: Decoder[OrderBook] = new Decoder[OrderBook] {
      final def apply(c: HCursor): Decoder.Result[OrderBook] =
        for {
          price <- c.downN(0).as[BigDecimal]
          lot <- c.downN(1).as[BigDecimal]
        } yield {
          OrderBook(price, lot)
        }
    }
  }

  sealed trait EventPayload
  case class OrderBookPayload(figi: String, depth: Int, bids: List[OrderBook], asks: List[OrderBook]) extends EventPayload

  case class OrderBookEvent(event: EventType, time: ZonedDateTime, payload: OrderBookPayload)
}
