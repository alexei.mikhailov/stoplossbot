package ml.tradeapi.model

import com.couchbase.client.scala.implicits.Codec
import io.circe.{Decoder, Encoder}
import io.circe.generic.extras.semiauto.{deriveEnumerationDecoder, deriveEnumerationEncoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

sealed trait PendingOrderType

object PendingOrderType {

  case object StopLoss extends PendingOrderType

  case object TakeProfit extends PendingOrderType

  implicit val decoder: Decoder[PendingOrderType] = deriveEnumerationDecoder
  implicit val encoder: Encoder[PendingOrderType] = deriveEnumerationEncoder
}


case class PendingOrder(figi: String, lots: BigDecimal, barrier: BigDecimal, orderType: PendingOrderType)

object PendingOrder {
  implicit val codec: Codec[PendingOrder] = Codec.codec[PendingOrder]

  implicit val decoder: Decoder[PendingOrder] = deriveDecoder[PendingOrder]
  implicit val encoder: Encoder[PendingOrder] = deriveEncoder[PendingOrder]
}



case class User(telegramId: Long, brokerAccountId: String, token: String, orders: List[PendingOrder],
                completedOrders: List[PendingOrder])

object User {
  implicit val codec: Codec[User] = Codec.codec[User]

  implicit val decoder: Decoder[User] = deriveDecoder[User]
  implicit val encoder: Encoder[User] = deriveEncoder[User]
}

