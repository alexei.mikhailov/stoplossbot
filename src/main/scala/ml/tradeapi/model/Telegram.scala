package ml.tradeapi.model

case class TelegramResponse(ok: Boolean, result: List[Update])

case class Update(update_id: Long, message: Option[Message])

case class Message(message_id: BigInt, from: Option[TelegramUser], chat: Chat, date: BigInt, text: Option[String])

case class TelegramUser(id: Long, is_bot: Boolean, first_name: String, username: Option[String], language_code: Option[String])

case class Chat(id: Long, first_name: String, username: String)
