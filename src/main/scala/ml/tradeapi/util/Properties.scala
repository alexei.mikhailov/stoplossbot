package ml.tradeapi.util

case object Properties {
  import pureconfig._
  import pureconfig.generic.auto._

  lazy val properties = ConfigSource.defaultApplication.loadOrThrow[Properties]
}

case class Properties(tinkoffApi: TinkoffApi, couchbase: Couchbase, telegram: Telegram)
case class TinkoffApi(url: String, streaming: String, token: String)
case class Couchbase(url: String, username: String, password: String, bucket: String)
case class Telegram(url: String, token: String)
