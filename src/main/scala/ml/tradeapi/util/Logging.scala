package ml.tradeapi.util

import cats.effect.{IO, Sync}
import org.slf4j._

// IO-based logger
object Logging {
  def apply(clazz: Class[_]): Logging = new Logging(LoggerFactory.getLogger(clazz))
}

class Logging(underlying: Logger) {
  def trace(message: String): IO[Unit] = IO(underlying.trace(message))
  def debug(message: String): IO[Unit] = IO(underlying.debug(message))
  def info(message: String): IO[Unit] = IO(underlying.info(message))
  def warn(message: String): IO[Unit] = IO(underlying.warn(message))
  def error(message: String): IO[Unit] = IO(underlying.error(message))
}
//
//// Typeclass-based logger
//object Log {
//  def apply[F[_]: Sync](clazz: Class[_]): Log[F] = new Log[F](LoggerFactory.getLogger(clazz))
//}
//
//class Log[F[_]: Sync](underlying: Logger) {
//  def info(message: String): F[Unit] = Sync[F].delay(underlying.info(message))
//}