package ml.tradeapi

import java.nio.ByteBuffer

import cats.effect.IO
import fs2.Stream
import sttp.client.SttpBackend
import sttp.client.asynchttpclient.WebSocketHandler

package object util {
  type sttpBackend = SttpBackend[IO, Stream[IO, ByteBuffer], WebSocketHandler]
}
