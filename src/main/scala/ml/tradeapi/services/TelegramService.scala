package ml.tradeapi.services

import ml.tradeapi.model.{TelegramResponse}
import ml.tradeapi.util.{Logging, Properties, sttpBackend}
import sttp.client._
import sttp.client.circe._
import io.circe.generic.auto._

object TelegramService {
  implicit val properties = Properties.properties.telegram

  def getUpdates(offset: BigInt)(implicit sttpBackend: sttpBackend) = {
    basicRequest.
      get(uri"${properties.url}/${properties.token}/getUpdates?offset=$offset").response(asJson[TelegramResponse]).
      send().
      map(_.body)
  }

  def sendMessage(chatId: String, message: String)(implicit sttpBackend: sttpBackend) = {
    basicRequest.
      get(uri"${properties.url}/${properties.token}/sendMessage?chat_id=$chatId&text=$message").
      send().
      map(_.body)
  }
}