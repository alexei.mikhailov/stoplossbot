package ml.tradeapi.services

import cats.effect.{ContextShift, IO}
import com.couchbase.client.scala.kv.MutateInResult
import io.circe.Json
import io.circe.syntax._
import ml.tradeapi.model.{Message, Payload, PendingOrder, User}
import ml.tradeapi.util.{Logging, sttpBackend}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

class UserServiceAsync

object UserServiceAsync {

  import scala.concurrent.ExecutionContext.global

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  val logging = Logging(classOf[UserServiceAsync])

  implicit val idGen: User => String = "user" + _.telegramId.toString

  def getUserSubscriptions(id: Long): IO[List[PendingOrder]] = {
    import com.couchbase.client.scala.kv.LookupInSpec._

    IO.fromFuture(IO(CouchbaseService.collection.
      lookupIn("user" + id.toString, Array(
        get("orders")
      )))).attempt.map{
        case Left(_) => None
        case Right(value) => value.contentAs[Json](0).toOption
      }.map(t => t.flatMap(_.as[List[PendingOrder]].toOption)).map(_.getOrElse(Nil))
  }

  def getAllUsers: IO[List[User]] = {
    import cats.implicits._
    for {
      view <- IO.fromFuture(IO(CouchbaseService.bucket.async.viewQuery("dev_ALL_KEYS", "VIEW_NAME")))
      ttt = view.rows.flatMap(_.id).filter(_.startsWith("user")).toList
      user <- ttt.map(CouchbaseService.get[User]).sequence
      filtered = user.flatMap(_.toList)

    } yield filtered
  }

  def completeOrder(id: Long, order: PendingOrder): IO[Either[Throwable, MutateInResult]] = {
    import com.couchbase.client.scala.kv.MutateInSpec._

    def removeOrd(order: PendingOrder, index: Int) = {
      logging.info(s"Remove ord with $order and $index")
      IO.fromFuture(IO(
        if (index >= 0)
          CouchbaseService.collection.mutateIn("user" + id.toString, Array(
            remove(s"orders[$index]"),
            arrayAppend("completedOrders", Seq(order.asJson))
          ))
        else
          Future.failed(new IllegalArgumentException)
      )).attempt
    }

    for {
      subscriptions <- getUserSubscriptions(id)
      ind = subscriptions.indexOf(order)
      result <- removeOrd(order, ind)
    } yield result
  }

  def getByTelegramId(id: Long): IO[Option[User]] =
    CouchbaseService.get[User]("user" + id.toString)

  def getUserFromMessage(message: Message): IO[Option[User]] =
    message.from.map(_.id) match {
      case Some(value) => getByTelegramId(value)
      case None =>IO.pure(None)
    }

  def createUser(userId: Long, token: String)(implicit backend: sttpBackend): IO[String] = {
    def storeUser(brokerAccountId: String) = {
      for {
        _ <- CouchbaseService.upsert(User(userId, brokerAccountId, token, Nil, Nil))
        userId <- getByTelegramId(userId)
        res = userId match {
          case Some(value) => value.toString
          case None => "User not found"
        }
      } yield res
    }

    for {
      apiResponse <- TinkofOpenApiAsyncService.createAccount(token)
      res <- apiResponse match {
        case Left(errorValue) => IO.pure(s"${errorValue.toString}: ${errorValue.body}")
        case Right(response) => response.payload match {
          case Payload.CreateAccount(brokerAccountType, brokerAccountId) => storeUser(brokerAccountId)
          case _ => IO.pure(s"error: unexpected payload from OpenApi")
        }
      }
    } yield res

  }

  def addPendingOrder(id: Long, order: PendingOrder): IO[MutateInResult] = {
    import com.couchbase.client.scala.kv.MutateInSpec._

    IO.fromFuture(IO(CouchbaseService.collection.mutateIn("user" + id.toString, Array(
      arrayAppend("orders", Seq(order.asJson))
    ))))
  }
}