package ml.tradeapi.services

import java.time.format.DateTimeFormatter
import java.time.{LocalDateTime, ZonedDateTime}

import cats.effect.{ContextShift, IO, Timer}
import ml.tradeapi.model.RequestTypes.{LimitOrder, MarketOrder, addCurrency}
import ml.tradeapi.model.{ApiResponse, Currency, OperationType, User}
import ml.tradeapi.util.{Properties, sttpBackend}
import sttp.client._
import sttp.client.circe._

object TinkofOpenApiAsyncService {

  import io.circe.generic.auto._
  import scala.concurrent.ExecutionContext.global

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO] = IO.timer(global)

  implicit val properties = Properties.properties.tinkoffApi

  private def authorizedRequest(token: String) = basicRequest.
    header("Authorization", s"Bearer $token")


  def createMarketOrder(user: User, figi: String, lot: BigDecimal, operation: OperationType)
                       (implicit backend: sttpBackend): IO[Either[io.circe.Error, ApiResponse]] = {
    import io.circe.parser.decode
    import io.circe.generic.auto._

    val request = authorizedRequest(user.token).
      post(uri"${properties.url}/orders/market-order?brokerAccountId=${user.brokerAccountId}&figi=$figi").
      body(MarketOrder(lot, operation))
    request.send().map(_.body match {
      case Left(value) => decode[ApiResponse](value)
      case Right(value) => decode[ApiResponse](value)
    })
  }

  def createAccount(token: String)
                   (implicit backend: sttpBackend): IO[Either[ResponseError[io.circe.Error], ApiResponse]] = {
    val request = authorizedRequest(token).
      post(uri"${properties.url}/sandbox/register").
      response(asJson[ApiResponse]).body("{\"brokerAccountType\": \"Tinkoff\"}")
    request.send().map(_.body)
  }

}

object TinkoffOpenApiService {
  implicit val backend = HttpURLConnectionBackend()
  implicit val properties = Properties.properties.tinkoffApi
  private val authorizedRequest = basicRequest.
    header("Authorization", s"Bearer ${properties.token}")

  private def req(token: String) = basicRequest.
    header("Authorization", s"Bearer $token")

  import io.circe.generic.auto._

  def getStocks() = {
    val request = authorizedRequest.
      get(uri"${properties.url}/market/stocks").response(asJson[ApiResponse])
    request.send().body
  }

  def getCurrencies() = {
    val request = authorizedRequest.
      get(uri"${properties.url}/market/currencies").response(asJson[ApiResponse])
    request.send().body
  }

  def createAccount(token: String): Either[ResponseError[io.circe.Error], ApiResponse] = {
    val request = req(token).
      post(uri"${properties.url}/sandbox/register").
      response(asJson[ApiResponse]).body("{\"brokerAccountType\": \"Tinkoff\"}")
    request.send().body
  }

  def getCurrencies(user: User) = {
    val request = req(user.token).
      get(uri"${properties.url}/portfolio/currencies?brokerAccountId=${user.brokerAccountId}").response(asJson[ApiResponse])
    request.send().body
  }

  def getPortfolio(user: User) = {
    val request = req(user.token).
      get(uri"${properties.url}/portfolio?brokerAccountId=${user.brokerAccountId}").response(asJson[ApiResponse])
    request.send().body
  }

  def getOperations(user: User, from: ZonedDateTime, to: ZonedDateTime) = {
    val formattedFrom = from.format(DateTimeFormatter.ISO_INSTANT)
    val formattedTo = to.format(DateTimeFormatter.ISO_INSTANT)
    val request = req(user.token).
      get(uri"${properties.url}/operations?brokerAccountId=${user.brokerAccountId}&from=$formattedFrom&to=$formattedTo").
      response(asJson[ApiResponse])
    request.send().body
  }

  def getOrders(user: User) = {
    val request = req(user.token).get(uri"${properties.url}/orders?brokerAccountId=${user.brokerAccountId}")
    request.send().body
  }

  def setCurrency(user: User, currency: Currency, balance: BigDecimal) = {
    val request = req(user.token).
      post(uri"${properties.url}/sandbox/currencies/balance?brokerAccountId=${user.brokerAccountId}").
      body(addCurrency(currency, balance)).
      response(asJson[ApiResponse])
    request.send().body.toOption
  }

  def createMarketOrder(user: User, figi: String, lot: BigDecimal, operation: OperationType) = {
    import io.circe.parser.decode
    import io.circe.generic.auto._

    val request = req(user.token).
      post(uri"${properties.url}/orders/market-order?brokerAccountId=${user.brokerAccountId}&figi=$figi").
      body(MarketOrder(lot, operation))
    request.send().body match {
      case Left(value) => decode[ApiResponse](value)
      case Right(value) => decode[ApiResponse](value)
    }
  }

  def createLimitOrder(user: User, figi: String, lot: BigDecimal, operation: OperationType, price: BigDecimal) = {
    import io.circe.parser.decode
    import io.circe.generic.auto._

    val request = req(user.token).
      post(uri"${properties.url}/orders/limit-order?brokerAccountId=${user.brokerAccountId}&figi=$figi").
      body(LimitOrder(lot, operation, price))
    //response(asJson[ApiResponse])
    request.send().body match {
      case Left(value) => decode[ApiResponse](value)
      case Right(value) => decode[ApiResponse](value)
    }
  }

  def getPositions(user: User) = {
    val request = req(user.token).
      get(uri"${properties.url}/portfolio?brokerAccountId=${user.brokerAccountId}")
    request.send().body
  }
}

