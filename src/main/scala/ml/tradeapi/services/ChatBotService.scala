package ml.tradeapi.services

import java.util.concurrent.atomic.AtomicLong

import cats.effect.{ContextShift, IO, Timer}
import ml.tradeapi.services.ChatBotCommands.Conversation
import ml.tradeapi.util.{Logging, sttpBackend}

import scala.collection.concurrent.TrieMap


trait OffsetStore {
  def get(implicit cs: ContextShift[IO]): IO[Long]

  def set(offest: Long)(implicit cs: ContextShift[IO]): IO[Unit]
}

class CouchBaseOffsetStore extends OffsetStore {

  import io.circe.generic.auto._

  case class Offset(id: Long)

  implicit val idGen: Offset => String = _ => "telegramOffset"

  def get(implicit cs: ContextShift[IO]): IO[Long] = {
    CouchbaseService.get[Offset]("telegramOffset").map(res => res.map(_.id).getOrElse(0L))
  }

  def set(offset: Long)(implicit cs: ContextShift[IO]): IO[Unit] =
    CouchbaseService.upsert(Offset(offset)).map(_ => ())
}

class SimpleChatService(offsetStore: OffsetStore) {
  private val conversations = TrieMap[Long, List[Conversation]]()

  import cats.implicits._
  import scala.concurrent.duration._
  import scala.concurrent.ExecutionContext.global

  val logging = Logging(classOf[SimpleChatService])

  implicit val timer: Timer[IO] = IO.timer(global)
  implicit val cs: ContextShift[IO] = IO.contextShift(global)

  def loop(implicit sttpBackend: sttpBackend): IO[Unit] = {

    for {
      latestUpdate <- offsetStore.get
      response <- TelegramService.getUpdates(latestUpdate)
      messagePairs <- response match {
        case Left(value) => IO.raiseError(value)
        case Right(value) => if (value.ok) {
          val parsedUpdates = for {
            updates <- value.result
            message <- updates.message
            from <- message.from
          } yield (updates.update_id, from.id, message)
          IO(parsedUpdates)
        } else
          IO.pure(Nil)
      }
      res <- messagePairs.map { message =>
        for {
          _ <- logging.info(s"update: ${message} offset:$latestUpdate")
          ctx <- ChatBotCommands.applyMessage(conversations.getOrElse(message._2, Nil), message._3)
          _ <- if (message._1 >= latestUpdate) offsetStore.set(message._1.toLong + 1) else IO.unit
          _ <- IO(conversations.put(message._2, ctx))
        } yield ctx
      }.sequence_
    } yield res
  }

  def simpleRun(implicit sttpBackend: sttpBackend): IO[Unit] =
    IO.sleep(300.millisecond) >> loop >> simpleRun
}