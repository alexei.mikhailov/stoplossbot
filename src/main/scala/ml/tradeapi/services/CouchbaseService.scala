package ml.tradeapi.services

import cats.effect.{ContextShift, IO}
import com.couchbase.client.scala.Cluster
import com.couchbase.client.scala.kv.MutationResult
import io.circe.{Decoder, Encoder, Json}
import io.circe.syntax._
import ml.tradeapi.util.{Logging, Properties}

object CouchbaseService {
  val properties = Properties.properties.couchbase

  lazy val cluster = Cluster.connect(properties.url, properties.username, properties.password).get
  lazy val bucket = cluster.bucket(properties.bucket)
  lazy val collection = bucket.defaultCollection.async

  def get[T](id: String)
            (implicit decoder: Decoder[T], cs: ContextShift[IO]): IO[Option[T]] = {
    IO.fromFuture(IO(collection.get(id))).attempt.map {
      case Left(_) => None
      case Right(value) => value.contentAs[Json].toOption.flatMap(_.as[T].toOption)
    }
  }

  def upsert[T](obj: T)
               (implicit encoder: Encoder[T], cs: ContextShift[IO], idGen: T => String): IO[Option[MutationResult]] = {
    IO.fromFuture(IO(collection.upsert(idGen(obj), obj.asJson))).attempt.map {
      case Left(_) => None
      case Right(value) => Some(value)
    }
  }
}

