package ml.tradeapi.services

import cats.effect.IO
import ml.tradeapi.model.{OperationType, OrderType, Payload, PendingOrder, PendingOrderType, User}
import ml.tradeapi.model.Streaming.{OrderBookEvent, OrderBookUnsubscribe}
import ml.tradeapi.streaming.HandlerIO
import ml.tradeapi.util.Logging

class StopLoss
object StopLoss {
  val loggingIO = Logging(classOf[StopLoss])

  def handleEventIO(handler: HandlerIO, orders: List[PendingOrder], event: OrderBookEvent): IO[Unit] = {
    import cats.implicits._
    implicit val backend = handler.backend
    val matchedOrders = checkConditions(orders, event)

    loggingIO.info(s"Got event: ${handler.user.telegramId}, $orders, $event") >>
    matchedOrders.map{ order =>
      for {
        _ <- loggingIO.info(s"Triggered order $order")
        _ <- UserServiceAsync.completeOrder(handler.user.telegramId, order)
        _ <- handler.unsubscribe(OrderBookUnsubscribe(figi = order.figi))
        marketOrder <- TinkofOpenApiAsyncService.createMarketOrder(handler.user, order.figi, order.lots, OperationType.Sell)
        instrument = ChatBotCommands.instruments.find(_.figi == order.figi).map(_.name).getOrElse("")
        _ <- TelegramService.sendMessage(handler.user.telegramId.toString,  s"Сработал ${order.orderType} по барьеру цены ${order.barrier} у бумаги $instrument")
        _ <- TelegramService.sendMessage(handler.user.telegramId.toString, ChatBotCommands.nicePrint(marketOrder))
      } yield IO.unit
    }.sequence_
  }

  def checkConditions(orders: List[PendingOrder], event: OrderBookEvent): List[PendingOrder] = {
    def isBarrierTriggered(order: PendingOrder) = order.orderType match {
      case PendingOrderType.StopLoss => event.payload.asks.map(_.price).exists(_ <= order.barrier)
      case PendingOrderType.TakeProfit => event.payload.bids.map(_.price).exists(_ >= order.barrier)
    }

    orders.filter(_.figi == event.payload.figi).filter(isBarrierTriggered)
  }


  def placeOrder(user: User, figi: String, operationType: OperationType, orderType: OrderType, amount: Int,
                 price: Option[BigDecimal] = None, barrierPrice: Option[BigDecimal] = None): IO[Either[String, String]] = {

    def placeDirectOrder(marketOperation: OperationType) = (orderType match {
      case OrderType.Limit => TinkoffOpenApiService.createLimitOrder(user, figi, amount, marketOperation, price.getOrElse(0))
      case OrderType.Market => TinkoffOpenApiService.createMarketOrder(user, figi, amount, marketOperation)
    }) match {
      case Left(value) => Left("Can't parse response from API")
      case Right(value) => value.payload match {
        case Payload.ErrorPayload(_, code) => Left(s"Order haven't been placed, reason: $code")
        case Payload.PlacedOrderPayload(orderId, operation, status, rejectReason, message, requestedLots, executedLots, commission) =>
          Right(s"Order has been placed")
        case _ => Left("Unexpected response from API")
      }
    }

    if (operationType == OperationType.BuyStopLoss || operationType == OperationType.BuyTakeProfit) {
      val pendingOrderType = if (operationType == OperationType.BuyTakeProfit) PendingOrderType.TakeProfit else PendingOrderType.StopLoss
      val order = PendingOrder(figi, amount, barrierPrice.getOrElse(0), pendingOrderType)

      for {
        _ <- UserServiceAsync.addPendingOrder(user.telegramId, order)
        directOrder = placeDirectOrder(OperationType.Buy)
      } yield directOrder
    } else {
      IO(placeDirectOrder(operationType))
    }

  }
}
