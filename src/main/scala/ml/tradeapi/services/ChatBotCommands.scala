package ml.tradeapi.services

import java.text.DecimalFormat
import java.time.format.DateTimeFormatter
import java.time.{LocalDateTime, ZonedDateTime}
import java.util.concurrent.atomic.AtomicReference

import cats.effect.IO
import cats.syntax._
import cats.implicits._
import ml.tradeapi.model.Payload.EmptyPayload
import ml.tradeapi.model.{ApiResponse, Currency, MarketInstrument, Message, OperationType, OrderType, Payload, User}
import ml.tradeapi.util.{Logging, sttpBackend}
import sttp.client.SttpBackend

import scala.util.Try

class ChatBotCommands

object ChatBotCommands {

  sealed trait Conversation {
    def action: Action
  }

  case class ImmediateAction(action: Action) extends Conversation

  case class InputAction(greeting: String, action: Action) extends Conversation

  type ConvCtx = List[Conversation]
  type Action = (ConvCtx, Message, sttpBackend) => IO[ConvCtx]

  val logging = Logging(classOf[ChatBotCommands])

  def replyIfSome(message: Message, text: Option[String])(implicit sttpBackend: sttpBackend) =
    text match {
      case Some(value) => replyIO(message, value)
      case None => IO.unit
    }

  def replyIO(message: Message, text: String)(implicit sttpBackend: sttpBackend): IO[Either[String, String]] =
    TelegramService.sendMessage(message.chat.id.toString, text)

  def getUserId(message: Message): IO[Option[Long]] = for {
    _ <- IO.unit
    id = for {
      from <- message.from
    } yield from.id
  } yield id

  def getUserIO(message: Message): IO[Option[User]] = for {
    telegramId <- getUserId(message)
    user <- telegramId match {
      case Some(value) => UserServiceAsync.getByTelegramId(value)
      case None => IO.pure(None)
    }
  } yield user

  def parseBigDecAndCurrency(str: String): Option[(BigDecimal, Currency)] = {
    val s = str.split(' ')
    if (s.length != 2)
      None
    else
      try {
        Some(BigDecimal(s.head), Currency.withNameInsensitive(s.tail.head))
      } catch {
        case _: Exception => None
      }
  }

  def respondIfNone[A, B](arg: Option[A], respond: => B): Option[A] = arg match {
    case Some(value) => Some(value)
    case None => {
      respond
      None
    }
  }

  val setBalance = InputAction("Укажите валюту и сумму. Например: \"100500 RUB\"",
    (ctx, message, sttpBackend) => for {

      userOpt <- UserServiceAsync.getUserFromMessage(message)
      opResult = userOpt.flatMap { user =>
        for {
          inputText <- message.text
          amountAndCurrency <- respondIfNone(parseBigDecAndCurrency(inputText), replyIO(message, "Ошибка формата")(sttpBackend))
          currencies <- TinkoffOpenApiService.setCurrency(user, amountAndCurrency._2, amountAndCurrency._1)
        } yield currencies
      }
      _ <- replyIfSome(message, opResult.map(nicePrint))(sttpBackend)
      _ <- replyIO(message, if (opResult.map(_.status).contains("Ok")) "Успешно" else "Не вполне успешно")(sttpBackend)
      next <- IO.pure(ctx.tail)
    } yield next
  )

  val showCurrencies = ImmediateAction((ctx, message, sttpBackend) => {
    for {
      userOpt <- UserServiceAsync.getUserFromMessage(message)
      opResult = userOpt.flatMap { user =>
        for {
          currencies <- TinkoffOpenApiService.getCurrencies(user).toOption
        } yield currencies
      }
      _ <- replyIfSome(message, opResult map nicePrint)(sttpBackend)
      next <- IO.pure(ctx.tail)
    } yield next
  })

  val showPorfolio = ImmediateAction((ctx, message, sttpBackend) => {
    for {
      userOpt <- UserServiceAsync.getUserFromMessage(message)
      opResult = userOpt.flatMap { user =>
        for {
          portfolio <- TinkoffOpenApiService.getPortfolio(user).toOption
        } yield portfolio
      }
      _ <- replyIfSome(message, opResult map nicePrint)(sttpBackend)
      next <- IO.pure(ctx.tail)
    } yield next
  })

  val showTradedCurrencies = ImmediateAction((ctx, message, sttpBackend) => {
    val result = for {
      portfolio <- TinkoffOpenApiService.getCurrencies().toOption
    } yield portfolio
    replyIO(message, result.toString)(sttpBackend) >> IO(ctx.tail)
  })


  val showOperations = ImmediateAction((ctx, message, sttpBackend) => {
    for {
      userOpt <- UserServiceAsync.getUserFromMessage(message)
      opResult = userOpt.flatMap { user =>
        for {
          operations <- TinkoffOpenApiService.getOperations(user,
            ZonedDateTime.now.minusDays(1),
            ZonedDateTime.now()
          ).toOption
        } yield operations
      }
      _ <- replyIfSome(message, opResult map nicePrint)(sttpBackend)
      next <- IO.pure(ctx.tail)
    } yield next
  })

  val showOrders = ImmediateAction((ctx, message, sttpBackend) => {
    for {
      userOpt <- UserServiceAsync.getUserFromMessage(message)
      opResult = userOpt.flatMap { user =>
        for {
          orders <- TinkoffOpenApiService.getOrders(user).toOption
        } yield orders
      }
      _ <- replyIfSome(message, opResult)(sttpBackend)
      next <- IO.pure(ctx.tail)
    } yield next
  })

  lazy val instruments = {
    TinkoffOpenApiService.getStocks().toOption match {
      case Some(value) => value.payload match {
        case Payload.InstrumentsResponse(instruments) => {
          instruments
        }
        case _ => Nil
      }
      case None => Nil
    }
  }


  val placeOrder = InputAction("Укажите тикер или часть названия бумаги. Например \"AAPL\" или \"Сбер\"",
    (ctx, message, _) => {
      def textCombined(instrument: MarketInstrument) =
        (instrument.figi + instrument.name + instrument.ticker).toLowerCase

      message.text.map { input =>
        for {
          instrument <- instruments if textCombined(instrument).contains(input.toLowerCase)
        } yield instrument
      } match {
        case Some(value) => {
          val selectedInstrument = new AtomicReference[Option[String]](None)
          val selectedOperation = new AtomicReference[Option[OperationType]](None)
          val selectedOrderType = new AtomicReference[Option[OrderType]](None)
          val selectedPrice = new AtomicReference[Option[BigDecimal]](None)
          val selectedBarrierPrice = new AtomicReference[Option[BigDecimal]](None)
          val selectedAmount = new AtomicReference[Option[Int]](None)

          val fillBarrier = inputStep[BigDecimal]("Укажите цену барьера, например \"100.5\"", selectedBarrierPrice.set)
          val fillPrice = inputStep[BigDecimal]("Укажите цену, например \"100.5\"", selectedPrice.set)

          def placeOrderWizard(user: User): IO[ConvCtx] = IO(
            searchConfirm(value.map(i => s"${i.name} (${i.ticker})" -> i.ticker).toMap, selectedInstrument.set) ::
              searchConfirm(Map(
                "Купить" -> OperationType.Buy,
                "Купить с ограничением потери(StopLoss)" -> OperationType.BuyStopLoss,
                "Купить с ограничением прибыли(TakeProfit)" -> OperationType.BuyTakeProfit,
                "Продать" -> OperationType.Sell
              ), selectedOperation.set,
                optionalAction(selectedOperation.get().exists(op => op == OperationType.BuyTakeProfit || op == OperationType.BuyStopLoss), fillBarrier)
              ) ::
              searchConfirm(Map(
                "По рыночной цене" -> OrderType.Market,
                "По фиксированной цене" -> OrderType.Limit
              ), selectedOrderType.set,
                optionalAction(selectedOrderType.get().contains(OrderType.Limit), fillPrice)
              ) ::
              inputStep[Int]("Укажите количество лотов, например \"1\"", selectedAmount.set) ::
              ImmediateAction((ctx, message, sttpBackend) => {
                val orderData = for {
                  from <- message.from
                  telegramId = from.id
                  instrument <- selectedInstrument.get()
                  figi <- instruments.find(_.ticker == instrument).map(_.figi)
                  operation <- selectedOperation.get()
                  amount <- selectedAmount.get()
                  orderType <- selectedOrderType.get()
                } yield (user, figi, operation, orderType, amount, selectedPrice.get(), selectedBarrierPrice.get())

                for {
                  placedOrder <- orderData match {
                    case Some(value) => StopLoss.placeOrder(value._1, value._2, value._3, value._4, value._5, value._6, value._7)
                    case None => IO.pure(Left("Can't parse inputs"))
                  }
                  _ <- replyIO(message,s"Выбран инструмент: ${selectedInstrument.get().getOrElse("...")}, " +
                    s"действие: ${selectedOperation.get()}, тип: ${selectedOrderType.get()}, цена: ${selectedPrice.get()}, " +
                    s"барьер: ${selectedBarrierPrice.get()}, количество: ${selectedAmount.get()}"
                  )(sttpBackend)
                  _ <- replyIO(message, placedOrder.toString)(sttpBackend)
                  next <- IO(ctx.tail)
                } yield next
              }) :: ctx.tail)

          for {
            userOpt <- UserServiceAsync.getUserFromMessage(message)
            opResult <- userOpt.map(placeOrderWizard) match {
              case Some(value) => value
              case None => IO.pure(ctx.tail)
            }
          } yield opResult
        }
        case None => IO(ctx.tail)
      }
    })

  def optionalAction(cond: => Boolean, action: Conversation): ConvCtx => ConvCtx = ctx => if (cond) action :: ctx.tail else ctx.tail

  def searchConfirm[A, B](options: Map[A, B], setSelection: Option[B] => Unit,
                          cont: ConvCtx => ConvCtx = ctx => ctx.tail): InputAction = {
    val zippedKeys = options.keys.zipWithIndex
    val zippedValues = options.values.zipWithIndex
    InputAction(s"Укажите подходящий вариант: \n" + zippedKeys.map(p => s"/${p._2} ${p._1}").mkString("\n"),
      (ctx, message, sttpBackend) => {
        message.text.map { input =>
          val checkedInput = Try {
            input.substring(1).toInt
          }.toOption.getOrElse(-1)
          zippedKeys.find(_._2 == checkedInput) match {
            case Some(value) => {
              setSelection(zippedValues.find(_._2 == input.substring(1).toInt).map(_._1))
              replyIO(message, s"Выбран ${value._1}")(sttpBackend) >> IO(cont(ctx))
            }
            case None => {
              replyIO(message, "Выбор нераспознан")(sttpBackend) >> IO(ctx)
            }
          }
        }.getOrElse(IO(ctx))
      })
  }

  implicit val stringToBigDecimal: String => Option[BigDecimal] = s => Try {
    BigDecimal(s)
  }.toOption
  implicit val stringToInt: String => Option[Int] = s => Try {
    s.toInt
  }.toOption

  def inputStep[A](greet: String, setSelection: Option[A] => Unit,
                   cont: ConvCtx => ConvCtx = ctx => ctx.tail)
                  (implicit conv: String => Option[A]): Conversation = {
    InputAction(greet,
      (ctx, message, sttpBackend) => {
        message.text.map { input =>
          conv(input) match {
            case Some(value) => {
              setSelection(Some(value))
              IO(cont(ctx))
            }
            case None => {
              replyIO(message, "Ввод нераспознан")(sttpBackend) >> IO(ctx)
            }
          }
        }
      }.getOrElse(IO(ctx)))
  }

  val greet = InputAction("Привет! Скажи что-нибудь", (ctx, message, sttpBackend) => {
    replyIO(message, s"Действительно, ${message.from.map(_.first_name).getOrElse("anon")}, ${message.text.getOrElse("")}")(sttpBackend) >>
    IO(ctx.tail)
  })

  val notAuthorized: InputAction = InputAction("Для продолжения работы необходимо зарегистрироваться. Укажите ваш api token",
    (ctx, message, backend) => {
      message.text.map { text =>
        val createdUser = for {
          token <- message.text.filter(_.startsWith("t."))
          from <- message.from
          telegramId = from.id
        } yield UserServiceAsync.createUser(telegramId, token)(backend)

        createdUser match {
          case Some(storedUser) => for {
            sss <- storedUser
            chk <- getUserIO(message)
            res <- if (chk.nonEmpty)
              IO.pure(List(menu))
            else
              replyIO(message, "Пользователь не зарегистрирован. " +
                "Для продолжения работы необходимо зарегистрироваться. " +
                "Укажите ваш api token")(backend).map(_ => ctx.tail)
          } yield res
          case None => IO.pure(List(notAuthorized))
        }
      }.getOrElse(IO(List(notAuthorized)))
    }
  )

  val menu = InputAction(
    """
      |Привет! Я умею:
      |1. Показывать:
      |   1.1 /currencies баланс по валютам
      |   1.2 /portfolio баланс по бумагам
      |   1.3 /orders список активных ордеров на бирже
      |   1.4 /operations список последних сделок
      |2. Проводить операции:
      |   2.1 /setBalance выставлять баланс в песочнице
      |   2.2 /placeOrder выставлять ордер
      |""".stripMargin,
    (ctx, message, _) => {
      IO(message.text.flatMap {
        case "/setBalance" => Some(setBalance)
        case "/currencies" => Some(showCurrencies)
        case "/portfolio" => Some(showPorfolio)
        case "/operations" => Some(showOperations)
        case "/orders" => Some(showOrders)
        case "/placeOrder" => Some(placeOrder)
        case "/hi" => Some(greet)
        case "/cur" => Some(showTradedCurrencies)
        case _ => None
      }.toList ++ ctx)
    })

  def applyMessage(convCtx: ConvCtx, message: Message)(implicit sttpBackend: sttpBackend): IO[ConvCtx] = {
    def apply(ctx: ConvCtx): IO[ConvCtx] = {
      for {
        conv <- if (ctx.isEmpty) IO(List(menu)) else IO(ctx)
        _ <- logging.debug(s"Got ctx: ${ctx.mkString("\n")}")
        _ <- logging.debug(s"Got message: ${message}")
        user <- getUserIO(message)
        authorized <- if (user.isEmpty) IO(List(notAuthorized)) else IO(conv)
        _ <- logging.debug(s"Got auth: ${authorized.mkString("\n")}")
        firstAction <- authorized.headOption match {
          case Some(action) => action.action(authorized, message, sttpBackend)
          case None => IO.pure(Nil)
        }
        _ <- logging.debug(s"Got actions: ${firstAction.mkString("\n")}")
        nextGreet <- firstAction.headOption match {
          case Some(action) => action match {
            case ImmediateAction(action) => apply(firstAction)
            case InputAction(greeting, action) => replyIO(message, greeting).map { _ => firstAction }
          }
          case None => IO.pure(firstAction)
        }
        _ <- logging.debug(s"Got following: ${nextGreet.mkString("\n")}")
      } yield nextGreet
    }

    apply(convCtx)
  }


  def formatBigDecimal(dec: BigDecimal) = {
    val df = new DecimalFormat("#,###.00")
    df.format(dec)
  }

  def nicePrint(response: Either[io.circe.Error, ApiResponse]): String = response match {
    case Left(value) => s"Ошибка: $value"
    case Right(value) => nicePrint(value)
  }

  def nicePrint(response: ApiResponse): String = response.payload match {
    case Payload.PortfolioCurrencies(currencies) =>
      currencies.map(c => s"${formatBigDecimal(c.balance)} ${c.currency}").mkString("\n")
    case Payload.CreateAccount(brokerAccountType, brokerAccountId) => "???"
    case Payload.ErrorPayload(message, code) => s"Произошла ошибка: $message"
    case EmptyPayload() => s"Пустой ответ"
    case Payload.PlacedOrderPayload(orderId, operation, status, rejectReason, message, requestedLots, executedLots, commission) =>
      rejectReason match {
        case Some(value) => s"Ордер отклнонен по причине $value"
        case None => s"Ордер выполнен со статусом $status запрошено $requestedLots удовлетворено $executedLots"
      }
    case Payload.OrderListPayload(orders) => "???"
    case Payload.InstrumentsResponse(instruments) => "???"
    case Payload.PortfolioResponse(portfolioPositions) =>
      portfolioPositions.map(p => s"${p.name} (${p.figi}): ${formatBigDecimal(p.balance)}").mkString("\n")
    case Payload.OperationsResponse(operations) => operations.take(10).map(op =>
      s"${op.figi} - ${op.operationType} статус:${op.status.getOrElse("-")} время:${op.date.format(DateTimeFormatter.ISO_TIME)}").mkString("\n")
  }
}
