package ml.tradeapi.services

import java.time.ZonedDateTime

import ml.tradeapi.model.{PendingOrder, PendingOrderType}
import ml.tradeapi.model.Streaming.{EventType, OrderBook, OrderBookEvent, OrderBookPayload}
import org.scalatest.{FlatSpec, Matchers}

class StopLossTest extends FlatSpec with Matchers {

  "Check conditions" should "return stopLoss if the barrier price exists in asks" in {
    val orders = List(
      PendingOrder("BBG000B9XRY4", 1, 200.0, PendingOrderType.StopLoss)
    )
    val event = OrderBookEvent(EventType.orderbook, ZonedDateTime.now, OrderBookPayload(
      "BBG000B9XRY4", 3, bids = Nil, asks = List(OrderBook(280, 1), OrderBook(200.0, 1))
    ))

    StopLoss.checkConditions(orders, event) shouldBe orders
  }

  "Check conditions" should "return takeProfit if the barrier price exists in bids" in {
    val orders = List(
      PendingOrder("BBG000B9XRY4", 1, 200.0, PendingOrderType.TakeProfit)
    )
    val event = OrderBookEvent(EventType.orderbook, ZonedDateTime.now, OrderBookPayload(
      "BBG000B9XRY4", 3, bids = List(OrderBook(280, 1)), asks = Nil
    ))

    StopLoss.checkConditions(orders, event) shouldBe orders
  }

  "Check conditions" should "match the FIGI for order" in {
    val orders = List(
      PendingOrder("BBG000B9XRY4", 1, 200.0, PendingOrderType.StopLoss),
      PendingOrder("BBG000B9XRY4", 2, 200.0, PendingOrderType.TakeProfit)
    )
    val event = OrderBookEvent(EventType.orderbook, ZonedDateTime.now, OrderBookPayload(
      "LOL", 3, bids = List(OrderBook(280, 1)), asks = List(OrderBook(190, 1))
    ))

    StopLoss.checkConditions(orders, event) shouldBe Nil
  }
}
